package pg.controller;

import properties_manager.PropertiesManager;
import static pg.LangProp.DEFAULT_IMAGE_CAPTION;
import static pg.LangProp.DEFAULT_PAGE_COLOR;
import static pg.LangProp.DEFAULT_PAGE_LAYOUT;
import static pg.LangProp.DEFAULT_PAGE_TITLE;
import static pg.StartConts.DEFAULT_PAGE_IMAGE;
import static pg.StartConts.PATH_PORT_IMAGES;
import pg.model.PortModel;
import pg.view.PortGenView;

/**
 * This controller provides responses for the slideshow edit toolbar,
 * which allows the user to add, remove, and reorder slides.
 * 
 * @author McKilla Gorilla & _____________
 */
public class PortEditController {
    // APP UI
    private PortGenView ui;
    
    /**
     * This constructor keeps the UI for later.
     */
    public PortEditController(PortGenView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * slide to the slide show.
     */
    public void processAddPageRequest() {
	PortModel portfolio = ui.getPortfolio();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	portfolio.addPage(props.getProperty(DEFAULT_PAGE_LAYOUT), props.getProperty(DEFAULT_PAGE_COLOR), props.getProperty(DEFAULT_PAGE_TITLE));
    }

    /**
     * Provides a response for when the user has selected a slide
     * and wishes to remove it from the slide show.
     */
    public void processRemovePageRequest() {
	PortModel portfolio = ui.getPortfolio();
	portfolio.removeSelectedPage();
	ui.reloadPortfolioPane();
    }

    /**
     * Provides a response for when the user has selected a slide
     * and wishes to move it up in the slide show.
     */
    public void processMovePageUpRequest() {
	PortModel portfolio = ui.getPortfolio();
	portfolio.moveSelectedPageUp();	
	
    }

    /**
     * Provides a response for when the user has selected a slide
     * and wises to move it down in the slide show.
     */
    public void processMovePageDownRequest() {
	PortModel portfolio = ui.getPortfolio();
	portfolio.moveSelectedPageDown();	
    }
}
