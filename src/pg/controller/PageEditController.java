package pg.controller;

import properties_manager.PropertiesManager;
import static pg.LangProp.DEFAULT_IMAGE_CAPTION;
import static pg.LangProp.DEFAULT_PAGE_COLOR;
import static pg.LangProp.DEFAULT_PAGE_LAYOUT;
import static pg.LangProp.DEFAULT_PAGE_TITLE;
import static pg.StartConts.DEFAULT_PAGE_IMAGE;
import static pg.StartConts.PATH_PORT_IMAGES;
import pg.model.PortModel;
import pg.view.PortGenView;

/**
 * This controller provides responses for the page edit toolbar,
 * which allows the user to add, remove, and reorder components.
 * 
 * @author McKilla Gorilla & _____________
 */
public class PageEditController {
    // APP UI
    private PortGenView ui;
    
    /**
     * This constructor keeps the UI for later.
     */
    public PageEditController(PortGenView initUI) {
	ui = initUI;
    }
    
    /**
     * Provides a response for when the user wishes to add a new
     * component to the page.
     */
    public void processAddTextRequest() {
	PortModel page = ui.getPortfolio();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	page.addPage(props.getProperty(DEFAULT_PAGE_LAYOUT), props.getProperty(DEFAULT_PAGE_COLOR), props.getProperty(DEFAULT_PAGE_TITLE));
    }

    /**
     * Provides a response for when the user has selected a component
     * and wishes to remove it from the page.
     */
    public void processRemoveTextRequest() {
	PortModel page = ui.getPortfolio();
	page.removeSelectedPage();
	ui.reloadPortfolioPane();
    }

   /**
     * Provides a response for when the user wishes to add a new
     * component to the page.
     */
    public void processAddImageRequest() {
	PortModel page = ui.getPortfolio();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	page.addPage(DEFAULT_PAGE_IMAGE, PATH_PORT_IMAGES, props.getProperty(DEFAULT_IMAGE_CAPTION));
    }

    /**
     * Provides a response for when the user has selected a component
     * and wishes to remove it from the page.
     */
    public void processRemoveImageRequest() {
	PortModel page = ui.getPortfolio();
	page.removeSelectedPage();
	ui.reloadPortfolioPane();
    }

       /**
     * Provides a response for when the user wishes to add a new
     * component to the page.
     */
    public void processAddVideoRequest() {
	PortModel page = ui.getPortfolio();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	page.addPage(DEFAULT_PAGE_IMAGE, PATH_PORT_IMAGES, props.getProperty(DEFAULT_IMAGE_CAPTION));
    }

    /**
     * Provides a response for when the user has selected a component
     * and wishes to remove it from the page.
     */
    public void processRemoveVideoRequest() {
	PortModel page = ui.getPortfolio();
	page.removeSelectedPage();
	ui.reloadPortfolioPane();
    }
    
   /**
     * Provides a response for when the user wishes to add a new
     * component to the page.
     */
    public void processAddSlideshowRequest() {
	PortModel page = ui.getPortfolio();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	page.addPage(DEFAULT_PAGE_IMAGE, PATH_PORT_IMAGES, props.getProperty(DEFAULT_IMAGE_CAPTION));
    }

    /**
     * Provides a response for when the user has selected a component
     * and wishes to remove it from the page.
     */
    public void processRemoveSlideshowRequest() {
	PortModel page = ui.getPortfolio();
	page.removeSelectedPage();
	ui.reloadPortfolioPane();
    }
}
