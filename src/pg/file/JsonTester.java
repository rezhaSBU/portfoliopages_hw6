package pg.file;

import java.io.IOException;
import static pg.StartConts.PATH_PORTS;
import pg.model.PortModel;
import static pg.file.PortFileManager.JSON_EXT;
import static pg.file.PortFileManager.SLASH;

/**
 *
 * @author McKillaGorilla
 */
public class JsonTester {
    static PortModel slideShow;
    static String TEST_TITLE = "The Test Portfolio";
    public static void main(String[] args) {
	slideShow = new PortModel(null);
	slideShow.setTitle(TEST_TITLE);
	
	// ADD THREE PAGES
	slideShow.addPage("Hello","HelloPic.png","./images/");
	slideShow.addPage("Clever Comment","DopeyPic.jpg","./images/pics/");
	slideShow.addPage("Goodbye","FinalPage.gif","./images/");
	
	PortFileManager fileManager = new PortFileManager();
	try {
	    fileManager.savePortfolio(slideShow);
	    System.out.println("PORTFOLIO SAVED");
	}
	catch(IOException ioe) {
	    ioe.printStackTrace();
	    System.exit(-1);
	}

	PortModel newPortfolio = new PortModel(null);
	try {
	    fileManager.loadPortfolio(slideShow, PATH_PORTS + SLASH + TEST_TITLE + JSON_EXT);
	    System.out.println("PORTFOLIO LOADED");
	}
	catch(IOException ioe) {
	    ioe.printStackTrace();
	    System.exit(-1);
	}
    }
}
