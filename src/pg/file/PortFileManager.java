package pg.file;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import static pg.StartConts.PATH_PORTS;
import pg.model.Page;
import pg.model.PortModel;

/**
 * This class uses the JSON standard to read and write pageshow data files.
 *
 * @author McKilla Gorilla & _____________
 */
public class PortFileManager {

    // JSON FILE READING AND WRITING CONSTANTS

    public static String JSON_TITLE = "title";
    public static String JSON_PAGES = "pages";
    public static String JSON_LAYOUT = "layout";
    public static String JSON_COLOR = "color";    
    public static String JSON_PAGE_TITLE = "page_title";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_CAPTION = "caption";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";

    /**
     * This method saves all the data associated with a portfolio to a JSON
     * file.
     *
     * @param portfolioToSave The course whose data we are saving.
     *
     * @throws IOException Thrown when there are issues writing to the JSON
     * file.
     */
    public void savePortfolio(PortModel portfolioToSave) throws IOException {
	StringWriter sw = new StringWriter();

	// BUILD THE PAGES ARRAY
	JsonArray pagesJsonArray = makePagesJsonArray(portfolioToSave.getPages());

	// NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
	JsonObject portfolioJsonObject = Json.createObjectBuilder()
		.add(JSON_TITLE, portfolioToSave.getTitle())
		.add(JSON_PAGES, pagesJsonArray)
		.build();

	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);

	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(portfolioJsonObject);
	jsonWriter.close();

	// INIT THE WRITER
	String portfolioTitle = "" + portfolioToSave.getTitle();
	String jsonFilePath = PATH_PORTS + SLASH + portfolioTitle + JSON_EXT;
	OutputStream os = new FileOutputStream(jsonFilePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(portfolioJsonObject);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(jsonFilePath);
	pw.write(prettyPrinted);
	pw.close();
	System.out.println(prettyPrinted);
    }

    /**
     * This method loads the contents of a JSON file representing a portfolio
     * into a PortfolioModel object.
     *
     * @param portfolioToLoad The portfolio to load
     * @param jsonFilePath The JSON file to load.
     * @throws IOException
     */
    public void loadPortfolio(PortModel portfolioToLoad, String jsonFilePath) throws IOException {
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(jsonFilePath);

	// NOW LOAD THE COURSE
	portfolioToLoad.reset();
	portfolioToLoad.setTitle(json.getString(JSON_TITLE));
	JsonArray jsonPagesArray = json.getJsonArray(JSON_PAGES);
	for (int i = 0; i < jsonPagesArray.size(); i++) {
	    JsonObject pageJso = jsonPagesArray.getJsonObject(i);
	    portfolioToLoad.addPage(pageJso.getString(JSON_LAYOUT),
		    pageJso.getString(JSON_COLOR),
		    pageJso.getString(JSON_PAGE_TITLE));
	}
    }

    // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
	JsonObject json = loadJSONFile(jsonFilePath);
	ArrayList<String> items = new ArrayList();
	JsonArray jsonArray = json.getJsonArray(arrayName);
	for (JsonValue jsV : jsonArray) {
	    items.add(jsV.toString());
	}
	return items;
    }

    private JsonArray makePagesJsonArray(List<Page> pages) {
	JsonArrayBuilder jsb = Json.createArrayBuilder();
	for (Page page : pages) {
	    JsonObject jso = makePageJsonObject(page);
	    jsb.add(jso);
	}
	JsonArray jA = jsb.build();
	return jA;
    }

    private JsonObject makePageJsonObject(Page page) {
	JsonObject jso = Json.createObjectBuilder()
		.add(JSON_LAYOUT, page.getPageLayout())
		.add(JSON_COLOR, page.getPageColor())
		.add(JSON_PAGE_TITLE, page.getTitle())
		.build();
	return jso;
    }
}
