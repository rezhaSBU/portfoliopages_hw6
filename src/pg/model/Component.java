package pg.model;

/**
 * This class represents a single slide in a slide show.
 * 
 * @author McKilla Gorilla & _____________
 */
public class Component {
    String type;
    String imageFileName;
    String imagePath;
    String caption;
    String banner_text;
    String banner_img;
    String header;
    String header_font_family;
    String header_font_style;
    String header_font_size;
    String text_type;
    String text_content;
    String text_font_family;
    String text_font_style;
    String text_font_size;
    String images;
    String video_name;
    String video_url;
    String sstitle;
    String footer;
    String footer_font_family;
    String footer_font_style;
    String footer_font_size;
    /**
     * Constructor, it initializes all slide data.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     * @param initCaption Textual caption to appear under the image.
     */
    public Component(String initImageFileName, String initImagePath, String initCaption) {
	imageFileName = initImageFileName;
	imagePath = initImagePath;
	caption = initCaption;
    }
////////////////////////////////////////////////////////////////////
    public Component(String initType) {
        type = initType;
    }
    
    // ACCESSOR METHODS
    public String getImageFileName() { return imageFileName; }
    public String getImagePath() { return imagePath; }
    public String getCaption() { return caption; }
//////////////////////////////////////////////////////////////////////    
    public String getType() { return type; }
    
    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
	imageFileName = initImageFileName;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setCaption(String initCaption) {
	caption = initCaption;
    }
    
    public void setImage(String initPath, String initFileName) {
	imagePath = initPath;
	imageFileName = initFileName;
    }
///////////////////////////////////////////////////////////////////    
    public void setType(String initType) {
	type = initType;
    }
    
}
