package pg.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pg.LangProp;
import pg.view.PortGenView;
import properties_manager.PropertiesManager;

/**
 * This class represents a single slide in a slide show.
 * 
 * @author McKilla Gorilla & Rezaul Hassan 108822849
 */
public class Page {
    String pageTitle;
    String pageURL;
    String layout;
    String color;
    String imageFileName;
    String imagePath;
    String caption;    
    PortGenView ui;
    ObservableList<Component> components;
    Component selectedComponent;
    /**
     * Constructor, it initializes all slide data.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     * @param initCaption Textual caption to appear under the image.
     */
    public Page(String initImageFileName, String initImagePath, String initCaption) {
	imageFileName = initImageFileName;
	imagePath = initImagePath;
	caption = initCaption;
    }
////////////////////////////////////////////////////////////////////
    public Page(String initPageTitle) {
        pageTitle = initPageTitle;
    }
    
    
    public Page(PortGenView initUI) {
	ui = initUI;
	components = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isComponentSelected() {
	return selectedComponent != null;
    }
    
    public boolean isSelectedComponent(Component testComponent) {
	return selectedComponent == testComponent;
    }
    
    public ObservableList<Component> getComponents() {
	return components;
    }
    
    public Component getSelectedComponents() {
	return selectedComponent;
    }

    
    // MUTATOR METHODS
    public void setSelectedComponent(Component initSelectedComponent) {
	selectedComponent = initSelectedComponent;
    }

    // SERVICE METHODS
    
    /**
     * Resets the portfolio to have no pages and a default title.
     */
    public void reset() {
	components.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	pageTitle = props.getProperty(LangProp.DEFAULT_PORT_TITLE);
	selectedComponent = null;
    }

    /**
     * Adds a page to the portfolio with the parameter settings.
     * @param initImageFileName File name of the page image to add.
     * @param initImagePath File path for the page image to add.
     * @param initCaption Caption for the page image to add.
     */
    public void addComponent(   String initImageFileName,
			    String initImagePath,
			    String initCaption) {
	Component componentToAdd = new Component(initImageFileName, initImagePath, initCaption);
	components.add(componentToAdd);
	ui.reloadPortfolioPane();
        ui.updateToolbarControls(false);
    }

    /**
     * Removes the currently selected page from the portfolio and
     * updates the display.
     */
    public void removeSelectedComponent() {
	if (isComponentSelected()) {
	    components.remove(selectedComponent);
	    selectedComponent = null;
	    ui.reloadPortfolioPane();
            ui.updateToolbarControls(false);
	}
    }
 
    /**
     * Moves the currently selected page up in the page
     * show by one page.
     */
    public void moveSelectedComponentUp() {
	if (isComponentSelected()) {
	    moveComponentUp(selectedComponent);
	    ui.reloadPortfolioPane();
            ui.updateToolbarControls(false);
	}
    }
    
    // HELPER METHOD
    private void moveComponentUp(Component componentToMove) {
	int index = components.indexOf(componentToMove);
	if (index > 0) {
	    Component temp = components.get(index);
	    components.set(index, components.get(index-1));
	    components.set(index-1, temp);
	}
    }
    
    /**
     * Moves the currently selected page down in the page
     * show by one page.
     */
    public void moveSelectedComponentDown() {
	if (isComponentSelected()) {
	    moveComponentDown(selectedComponent);
	    ui.reloadPortfolioPane();
            ui.updateToolbarControls(false);
	}
    }
    
    // HELPER METHOD
    private void moveComponentDown(Component componentToMove) {
	int index = components.indexOf(componentToMove);
	if (index < (components.size()-1)) {
	    Component temp = components.get(index);
	    components.set(index, components.get(index+1));
	    components.set(index+1, temp);
	}
    }
    
    /**
     * Changes the currently selected page to the previous page
     * in the portfolio.
     */
    public void previous() {
	if (selectedComponent == null)
	    return;
	else {
	    int index = components.indexOf(selectedComponent);
	    index--;
	    if (index < 0)
		index = components.size() - 1;
	    selectedComponent = components.get(index);
	}
    }

    /**
     * Changes the currently selected page to the next page
     * in the portfolio.
     */    
    public void next() {
    	if (selectedComponent == null)
	    return;
	else {
	    int index = components.indexOf(selectedComponent);
	    index++;
	    if (index >= components.size())
		index = 0;
	    selectedComponent = components.get(index);
	}
    }    
    // ACCESSOR METHODS
    public String getImageFileName() { return imageFileName; }
    public String getImagePath() { return imagePath; }
    public String getCaption() { return caption; }
//////////////////////////////////////////////////////////////////////    
    public String getPageLayout() { return layout; }
    public String getPageColor() { return color; }
    public String getTitle() { return pageTitle; }
    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
	imageFileName = initImageFileName;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setCaption(String initCaption) {
	caption = initCaption;
    }
    
    public void setImage(String initPath, String initFileName) {
	imagePath = initPath;
	imageFileName = initFileName;
    }
    
    public void setTitle(String initTitle) { 
	pageTitle = initTitle; 
    }
}
