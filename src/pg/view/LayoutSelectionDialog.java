package pg.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static pg.StartConts.CSS_CLASS_BOXES;
import static pg.StartConts.CSS_CLASS_BUTN;
import static pg.StartConts.CSS_CLASS_DIALOG_BOX;
import static pg.StartConts.ENGLISH_LANG;
import static pg.StartConts.SPANISH_LANG;
import static pg.StartConts.LABEL_LANGUAGE_SELECTION_PROMPT;
import static pg.StartConts.OK_BUTTON_TEXT;
import static pg.StartConts.STYLE_SHEET_UI;

/**
 *
 * @author McKillaGorilla
 */
public class LayoutSelectionDialog extends Stage {
    GridPane gBox;
    VBox vBox;
    Label layoutPromptLabel;
    ComboBox layoutComboBox;
    Button okButton;
    String selectedLayout = "Nav centered under banner";
    
    public LayoutSelectionDialog() {
	layoutPromptLabel = new Label("Select a layout Scheme");
	
	// INIT THE LANGUAGE CHOICES
	ObservableList<String> layoutChoices = FXCollections.observableArrayList();
	layoutChoices.add("Nav centered under banner");
	layoutChoices.add("Nav down along Left");
	layoutChoices.add("Nav at Top Left");
	layoutChoices.add("Nav at Top Right");
	layoutChoices.add("Nav down along Right");
	layoutComboBox = new ComboBox(layoutChoices);
        layoutComboBox.getSelectionModel().select("Nav centered under banner");
	okButton = new Button(OK_BUTTON_TEXT);
	
	gBox = new GridPane();
        vBox = new VBox(20);
        gBox.setVgap(8);
        gBox.setHgap(8);
	vBox.getChildren().add(layoutPromptLabel);
	vBox.getChildren().add(layoutComboBox);
	gBox.add(vBox, 0, 0, 2, 1);
        gBox.add(okButton, 1, 2);
        GridPane.setHalignment(okButton, HPos.RIGHT);
	okButton.setOnAction(e -> {
	    selectedLayout = layoutComboBox.getSelectionModel().getSelectedItem().toString();
	    this.hide();
	});
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gBox);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        gBox.getStyleClass().add(CSS_CLASS_DIALOG_BOX);
        vBox.getStyleClass().add(CSS_CLASS_BOXES);
	layoutComboBox.getStyleClass().add(CSS_CLASS_BUTN);
	okButton.getStyleClass().add(CSS_CLASS_BUTN);
	this.setTitle("Layout Scheme Selection");
	setScene(scene);
    }
    
    public String getSelectedLayout() {
	return selectedLayout;
    }
}
