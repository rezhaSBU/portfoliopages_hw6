package pg.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static pg.StartConts.CSS_CLASS_BOXES;
import static pg.StartConts.CSS_CLASS_BUTN;
import static pg.StartConts.CSS_CLASS_DIALOG_BOX;
import static pg.StartConts.ENGLISH_LANG;
import static pg.StartConts.SPANISH_LANG;
import static pg.StartConts.LABEL_LANGUAGE_SELECTION_PROMPT;
import static pg.StartConts.OK_BUTTON_TEXT;
import static pg.StartConts.STYLE_SHEET_UI;

/**
 *
 * @author McKillaGorilla
 */
public class FontSelectionDialog extends Stage {
    GridPane gBox;
    VBox vBox;
    Label fontPromptLabel;
    ComboBox fontComboBox;
    Button okButton;
    String selectedFont = "Lucida Grande";
    
    public FontSelectionDialog() {
	fontPromptLabel = new Label("Select a font Scheme");
	
	// INIT THE LANGUAGE CHOICES
	ObservableList<String> fontChoices = FXCollections.observableArrayList();
	fontChoices.add("Lemon");
	fontChoices.add("Nunito 400");
	fontChoices.add("Nunito 700");
	fontChoices.add("Alegreya Sans");
	fontChoices.add("Cagliostro");
	fontChoices.add("Cinzel");
	fontChoices.add("Berkshire Swash");
	fontChoices.add("Sirin Stencil");
	fontChoices.add("Asul");
	fontChoices.add("Walter Turncoat");
	fontChoices.add("Arbutus");
	fontChoices.add("Noticia Text");
	fontComboBox = new ComboBox(fontChoices);
        fontComboBox.getSelectionModel().select("Stony Brook");

        okButton = new Button(OK_BUTTON_TEXT);
	
	gBox = new GridPane();
        vBox = new VBox(20);
        gBox.setVgap(8);
        gBox.setHgap(8);
	vBox.getChildren().add(fontPromptLabel);
	vBox.getChildren().add(fontComboBox);
	gBox.add(vBox, 0, 0, 2, 1);
        gBox.add(okButton, 1, 2);
        GridPane.setHalignment(okButton, HPos.RIGHT);
	okButton.setOnAction(e -> {
	    selectedFont = fontComboBox.getSelectionModel().getSelectedItem().toString();
	    this.hide();
	});
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gBox);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        gBox.getStyleClass().add(CSS_CLASS_DIALOG_BOX);
        vBox.getStyleClass().add(CSS_CLASS_BOXES);
	fontComboBox.getStyleClass().add(CSS_CLASS_BUTN);
	okButton.getStyleClass().add(CSS_CLASS_BUTN);
	this.setTitle("Font Scheme Selection");
	setScene(scene);
    }
    
    public String getSelectedFont() {
	return selectedFont;
    }
}
