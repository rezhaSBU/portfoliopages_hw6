package pg.view;

import java.io.File;
import java.net.URL;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import pg.LangProp;
import static pg.StartConts.CSS_CLASS_PAGE_EDIT_VIEW;
import static pg.StartConts.DEFAULT_THUMBNAIL_WIDTH;
import pg.controller.ImageSelectionController;
import pg.error.ErrorHandler;
import pg.model.Page;
import static pg.file.PortFileManager.SLASH;

/**
 * This UI component has the controls for editing a single page
 * in a portfolio, including controls for selecting the page components
 * and changing the components.
 * 
 * @author McKilla Gorilla & Rezaul Hassan 108822849
 */
public class PageEditView extends HBox {
    // PAGE THIS COMPONENT EDITS
    Page page;
    
    // CONTROLS FOR EDITING THE TITLE
    VBox titleVBox;
    Label titleLabel;
    TextField titleTextField;
    
    // DISPLAYS THE IMAGE FOR THIS PAGE
//    ImageView imageSelectionView;
//    
//    // CONTROLS FOR EDITING THE CAPTION
//    VBox captionVBox;
//    Label captionLabel;
//    TextField captionTextField;
//    
//    // PROVIDES RESPONSES FOR IMAGE SELECTION
//    ImageSelectionController imageController;

    /**
     * THis constructor initializes the full UI for this component, using
     * the initPage data for initializing values./
     * 
     * @param initPage The page to be edited by this component.
     */
    public PageEditView(Page initPage) {
	// FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	this.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);
	
	// KEEP THE PAGE FOR LATER
	page = initPage;
        
        //SETUP THE TITLE CONTROL
	titleVBox = new VBox();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	titleLabel = new Label(props.getProperty(LangProp.LABEL_PORT_TITLE));
	titleTextField = new TextField();
	titleTextField.setText(props.getProperty(LangProp.DEFAULT_PAGE_TITLE));
	titleVBox.getChildren().add(titleLabel);
	titleVBox.getChildren().add(titleTextField);
        getChildren().add(titleVBox);
        titleTextField.textProperty().addListener(e -> {
	    String text = titleTextField.getText();
	    page.setTitle(text);	    
	});
//	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
//	imageSelectionView = new ImageView();
//	updateSlideImage();
//
//	// SETUP THE CAPTION CONTROLS
//	captionVBox = new VBox();
//	PropertiesManager props = PropertiesManager.getPropertiesManager();
//	captionLabel = new Label(props.getProperty(LangProp.LABEL_CAPTION));
//	captionTextField = new TextField();
//	captionTextField.setText(page.getCaption());
//	captionVBox.getChildren().add(captionLabel);
//	captionVBox.getChildren().add(captionTextField);
//
//	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
//	setContent(imageSelectionView);
//	setContent(captionVBox);
//
//	// SETUP THE EVENT HANDLERS
//	imageController = new ImageSelectionController();
//	imageSelectionView.setOnMousePressed(e -> {
//	    imageController.processSelectImage(page, this);
//	});
//	captionTextField.textProperty().addListener(e -> {
//	    String text = captionTextField.getText();
//	    page.setCaption(text);	    
//	});
    }
    
    /**
     * This function gets the image for the page and uses it to
     * update the image displayed.
     */
    public void updateSlideImage() {
//	String imagePath = page.getImagePath() + SLASH + page.getImageFileName();
//	File file = new File(imagePath);
//	try {
//	    // GET AND SET THE IMAGE
//	    URL fileURL = file.toURI().toURL();
//	    Image pageImage = new Image(fileURL.toExternalForm());
//	    imageSelectionView.setImage(pageImage);
//	    
//	    // AND RESIZE IT
//	    double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
//	    double perc = scaledWidth / pageImage.getWidth();
//	    double scaledHeight = pageImage.getHeight() * perc;
//	    imageSelectionView.setFitWidth(scaledWidth);
//	    imageSelectionView.setFitHeight(scaledHeight);
//	} catch (Exception e) {
//	    ErrorHandler eH = new ErrorHandler(null);
//            eH.processError(LangProp.ERROR_UNEXPECTED);
//	}
    }    

}