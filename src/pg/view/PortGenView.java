package pg.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import pg.LangProp;
import static pg.LangProp.LABEL_PORT_TITLE;
import static pg.LangProp.TOOLTIP_ADD_IMAGE;
import static pg.LangProp.TOOLTIP_ADD_PAGE;
import static pg.LangProp.TOOLTIP_ADD_SLIDESHOW;
import static pg.LangProp.TOOLTIP_ADD_TEXT;
import static pg.LangProp.TOOLTIP_ADD_VIDEO;
import static pg.LangProp.TOOLTIP_CHOOSE_COLOR;
import static pg.LangProp.TOOLTIP_CHOOSE_FONT;
import static pg.LangProp.TOOLTIP_CHOOSE_LAYOUT;
import static pg.LangProp.TOOLTIP_EXIT;
import static pg.LangProp.TOOLTIP_LOAD_PORT;
import static pg.LangProp.TOOLTIP_MOVE_DOWN;
import static pg.LangProp.TOOLTIP_MOVE_UP;
import static pg.LangProp.TOOLTIP_NEW_PORT;
import static pg.LangProp.TOOLTIP_REMOVE_PAGE;
import static pg.LangProp.TOOLTIP_SAVE_PORT;
import static pg.LangProp.TOOLTIP_EXPORT_PORT;
import static pg.LangProp.TOOLTIP_REMOVE_IMAGE;
import static pg.LangProp.TOOLTIP_REMOVE_SLIDESHOW;
import static pg.LangProp.TOOLTIP_REMOVE_TEXT;
import static pg.LangProp.TOOLTIP_REMOVE_VIDEO;
import static pg.LangProp.TOOLTIP_SAVE_AS_PORT;
import static pg.StartConts.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static pg.StartConts.CSS_CLASS_HORIZONTAL_TOOLBAR_MASTER;
import static pg.StartConts.CSS_CLASS_SELECTED_PAGE_EDIT_VIEW;
import static pg.StartConts.CSS_CLASS_PAGE_EDIT_VIEW;
import static pg.StartConts.CSS_CLASS_PORT_EDIT_VBOX;
import static pg.StartConts.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static pg.StartConts.ICON_ADD_IMAGE;
import static pg.StartConts.ICON_ADD_PAGE;
import static pg.StartConts.ICON_ADD_SLIDESHOW;
import static pg.StartConts.ICON_ADD_TEXT;
import static pg.StartConts.ICON_ADD_VIDEO;
import static pg.StartConts.ICON_CHOOSE_COLOR;
import static pg.StartConts.ICON_CHOOSE_FONT;
import static pg.StartConts.ICON_CHOOSE_LAYOUT;
import static pg.StartConts.ICON_EXIT;
import static pg.StartConts.ICON_EXPORT_PORT;
import static pg.StartConts.ICON_LOAD_PORT;
import static pg.StartConts.ICON_MOVE_DOWN;
import static pg.StartConts.ICON_MOVE_UP;
import static pg.StartConts.ICON_NEW_PORT;
import static pg.StartConts.ICON_REMOVE_IMAGE;
import static pg.StartConts.ICON_REMOVE_PAGE;
import static pg.StartConts.ICON_REMOVE_SLIDESHOW;
import static pg.StartConts.ICON_REMOVE_TEXT;
import static pg.StartConts.ICON_REMOVE_VIDEO;
import static pg.StartConts.ICON_SAVE_AS_PORT;
import static pg.StartConts.ICON_SAVE_PORT;
import static pg.StartConts.ICON_VIEW_PORT;
import static pg.StartConts.PATH_ICONS;
import static pg.StartConts.STYLE_SHEET_UI;
import pg.controller.FileController;
import pg.controller.PageEditController;
import pg.controller.PortEditController;
import pg.model.Page;
import pg.model.PortModel;
import pg.error.ErrorHandler;
import pg.file.PortFileManager;

/**
 * This class provides the User Interface for this application,
 * providing controls and the entry points for creating, loading, 
 * saving, editing, and viewing page shows.
 * 
 * @author McKilla Gorilla & Rezaul Hassan 108822849
 */
public class PortGenView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane pgPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newPortfolioButton;
    Button loadPortfolioButton;
    Button savePortfolioButton;
    Button saveasPortfolioButton;
    Button exportPortfolioButton;
    Button exitButton;
    // FOR THE PORTFOLIO TITLE
    FlowPane titlePane;
    Label titleLabel;
    TextField titleTextField;
    
    // WORKSPACE
    GridPane workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox pageEditToolbar;
    
    HBox siteToolbar;
    Label siteLabel;
    Button addPageButton;
    Button removePageButton;
    Button movePageUpButton;
    Button movePageDownButton;
    
    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    FlowPane componentToolbox;
    Label componentLabel;
    Button chooseColorButton;
    Button chooseLayoutButton;
    Button chooseFontButton;    
    Button addTextButton;
    Button removeTextButton;
    Button addImageButton;
    Button removeImageButton;
    Button addVideoButton;
    Button removeVideoButton;
    Button addSlideshowButton;
    Button removeSlideshowButton;
    
    // AND THIS WILL GO IN THE CENTER
    ScrollPane pagesEditorBorderPane;
    VBox pagesEditorPane;

    // THIS IS THE PORTFOLIO WE'RE WORKING WITH
    PortModel portfolio;

    // THIS IS FOR SAVING AND LOADING PORTFOLIOS
    PortFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO PORTFOLIO EDIT BUTTONS
    private PortEditController editController;

    // THIS CONTROLLER RESPONDS TO COMPONENT EDIT BUTTONS
    private PageEditController pageController;


    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public PortGenView(PortFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// MAKE THE DATA MANAGING MODEL
	portfolio = new PortModel(this);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
    }

    // ACCESSOR METHODS
    public PortModel getPortfolio() {
	return portfolio;
    }

    public Stage getWindow() {
	return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle);
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new GridPane();
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	siteToolbar = new HBox();
//	siteToolbar.getStyleClass().add(CSS_CLASS_PORT_EDIT_VBOX);
        siteLabel = new Label("Pages     ");
        addPageButton = this.initChildButton(siteToolbar,   ICON_ADD_PAGE,  TOOLTIP_ADD_PAGE,   CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	removePageButton = this.initChildButton(siteToolbar,    ICON_REMOVE_PAGE,   TOOLTIP_REMOVE_PAGE,    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	movePageUpButton = this.initChildButton(siteToolbar,    ICON_MOVE_UP,   TOOLTIP_MOVE_UP,    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	movePageDownButton = this.initChildButton(siteToolbar,  ICON_MOVE_DOWN, TOOLTIP_MOVE_DOWN,  CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	
        //THIS WILL GO ON THE LEFT SIDE OF THE SCREEN
        componentToolbox = new FlowPane();
        componentLabel = new Label("Components");
        chooseColorButton = this.initChildButton(componentToolbox,  ICON_CHOOSE_COLOR,  TOOLTIP_CHOOSE_COLOR,  CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        chooseLayoutButton = this.initChildButton(componentToolbox,  ICON_CHOOSE_LAYOUT,  TOOLTIP_CHOOSE_LAYOUT,  CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        chooseFontButton = this.initChildButton(componentToolbox,  ICON_CHOOSE_FONT,  TOOLTIP_CHOOSE_FONT,  CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        addTextButton = this.initChildButton(componentToolbox,  ICON_ADD_TEXT,  TOOLTIP_ADD_TEXT,  CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        removeTextButton = this.initChildButton(componentToolbox,  ICON_REMOVE_TEXT,  TOOLTIP_REMOVE_TEXT,  CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        addImageButton = this.initChildButton(componentToolbox,  ICON_ADD_IMAGE,  TOOLTIP_ADD_IMAGE,  CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        removeImageButton = this.initChildButton(componentToolbox,  ICON_REMOVE_IMAGE,  TOOLTIP_REMOVE_IMAGE,  CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        addVideoButton = this.initChildButton(componentToolbox,  ICON_ADD_VIDEO,  TOOLTIP_ADD_VIDEO,  CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        removeVideoButton = this.initChildButton(componentToolbox,  ICON_REMOVE_VIDEO,  TOOLTIP_REMOVE_VIDEO,  CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        addSlideshowButton = this.initChildButton(componentToolbox,  ICON_ADD_SLIDESHOW,  TOOLTIP_ADD_SLIDESHOW,  CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        removeSlideshowButton = this.initChildButton(componentToolbox,  ICON_REMOVE_SLIDESHOW,  TOOLTIP_REMOVE_SLIDESHOW,  CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
        
        pageEditToolbar = new VBox();
        pageEditToolbar.getChildren().add(siteLabel);
        pageEditToolbar.getChildren().add(siteToolbar);
        pageEditToolbar.getChildren().add(componentLabel);
        pageEditToolbar.getChildren().add(componentToolbox);

        // AND THIS WILL GO IN THE CENTER
	pagesEditorPane = new VBox();
	pagesEditorBorderPane = new ScrollPane(pagesEditorPane);
	initTitleControls();
	
	// NOW PUT THESE TWO IN THE WORKSPACE
	workspace.add(pageEditToolbar, 0, 0);
//        workspace.add(titlePane, 1, 0);
//        workspace.add(componentToolbox, 0, 1);
        workspace.add(pagesEditorBorderPane, 1, 0);
    }

    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	fileController = new FileController(this, fileManager);
	newPortfolioButton.setOnAction(e -> {
	    fileController.handleNewPortfolioRequest();
	});
	loadPortfolioButton.setOnAction(e -> {
	    fileController.handleLoadPortfolioRequest();
	});
	savePortfolioButton.setOnAction(e -> {
	    fileController.handleSavePortfolioRequest();
	});
	saveasPortfolioButton.setOnAction(e -> {
	    fileController.handleSavePortfolioRequest();
	});
	exportPortfolioButton.setOnAction(e -> {
	    fileController.handleExportPortfolioRequest();
	});
	exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
	});
	
	// THEN THE PORTFOLIO EDIT CONTROLS
	editController = new PortEditController(this);
	addPageButton.setOnAction(e -> {
	    editController.processAddPageRequest();
	});
	removePageButton.setOnAction(e -> {
	    editController.processRemovePageRequest();
	});
	movePageUpButton.setOnAction(e -> {
	    editController.processMovePageUpRequest();
	});
	movePageDownButton.setOnAction(e -> {
	    editController.processMovePageDownRequest();
	});
        
        // THEN THE PAGE EDIT CONTROLS
	pageController = new PageEditController(this);
	chooseColorButton.setOnAction(e -> {
	    ColorSelectionDialog colorDialog = new ColorSelectionDialog();
            colorDialog.showAndWait();
	});
	chooseLayoutButton.setOnAction(e -> {
	    LayoutSelectionDialog layoutDialog = new LayoutSelectionDialog();
            layoutDialog.showAndWait();
	});
	chooseFontButton.setOnAction(e -> {
	    FontSelectionDialog fontDialog = new FontSelectionDialog();
            fontDialog.showAndWait();
	});	
        addTextButton.setOnAction(e -> {
	    TextDialog textDialog = new TextDialog();
            textDialog.showAndWait();
	});
	removeTextButton.setOnAction(e -> {
	    pageController.processRemoveTextRequest();
	});
	addImageButton.setOnAction(e -> {
	    ImageDialog imageDialog = new ImageDialog();
            imageDialog.showAndWait();
	});
	removeImageButton.setOnAction(e -> {
	    pageController.processRemoveImageRequest();
	});
	addVideoButton.setOnAction(e -> {
	    VideoDialog videoDialog = new VideoDialog();
            videoDialog.showAndWait();
	});
	removeVideoButton.setOnAction(e -> {
	    pageController.processRemoveVideoRequest();
	});
	addSlideshowButton.setOnAction(e -> {
	    SlideshowDialog slideshowDialog = new SlideshowDialog();
            slideshowDialog.showAndWait();
	});
	removeSlideshowButton.setOnAction(e -> {
	    pageController.processRemoveSlideshowRequest();
	});

    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
        fileToolbarPane.getStyleClass().add(CSS_CLASS_HORIZONTAL_TOOLBAR_MASTER);
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	newPortfolioButton = initChildButton(fileToolbarPane, ICON_NEW_PORT,    TOOLTIP_NEW_PORT,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadPortfolioButton = initChildButton(fileToolbarPane, ICON_LOAD_PORT,	TOOLTIP_LOAD_PORT,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	savePortfolioButton = initChildButton(fileToolbarPane, ICON_SAVE_PORT,	TOOLTIP_SAVE_PORT,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	saveasPortfolioButton = initChildButton(fileToolbarPane, ICON_SAVE_AS_PORT, TOOLTIP_SAVE_AS_PORT,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	exportPortfolioButton = initChildButton(fileToolbarPane, ICON_EXPORT_PORT,  TOOLTIP_EXPORT_PORT,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    }

    private void initWindow(String windowTitle) {
	// SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX()+250);
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth()*0.5);
	primaryStage.setHeight(bounds.getHeight()*0.85);

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	pgPane = new BorderPane();
	pgPane.setTop(fileToolbarPane);
	primaryScene = new Scene(pgPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LangProp tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
	Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;
    }
    
    /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	pgPane.setCenter(workspace);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	savePortfolioButton.setDisable(saved);
        saveasPortfolioButton.setDisable(saved);
	exportPortfolioButton.setDisable(false);
	
	updatePageEditToolbarControls();
    }
    
    public void updatePageEditToolbarControls() {
	// AND THE PORTFOLIO EDIT TOOLBAR
	addPageButton.setDisable(false);
	boolean pageSelected = portfolio.isPageSelected();
	removePageButton.setDisable(!pageSelected);
	movePageUpButton.setDisable(!(pageSelected & portfolio.getPages().size() > 1));
	movePageDownButton.setDisable(!(pageSelected & portfolio.getPages().size() > 1));
        //AND THE PAGE EDIT TOOLBAR
        chooseColorButton.setDisable(false);
        chooseLayoutButton.setDisable(false);
        chooseFontButton.setDisable(false);
        addTextButton.setDisable(false);
        removeTextButton.setDisable(false);
        addImageButton.setDisable(false);
        removeImageButton.setDisable(false);
        addVideoButton.setDisable(false);
        removeVideoButton.setDisable(false);
        addSlideshowButton.setDisable(false);
        removeSlideshowButton.setDisable(false);
    }

    /**
     * Uses the page show data to reload all the components for
     * page editing.
     * 
     * @param portfolioToLoad SLide show being reloaded.
     */
    public void reloadPortfolioPane() {
	pagesEditorPane.getChildren().clear();
	for (Page page : portfolio.getPages()) {
	    PageEditView siteor = new PageEditView(page);
	    if (portfolio.isSelectedPage(page))
		siteor.getStyleClass().add(CSS_CLASS_PAGE_EDIT_VIEW);
	    else
		siteor.getStyleClass().add(CSS_CLASS_SELECTED_PAGE_EDIT_VIEW);
	    pagesEditorPane.getChildren().add(siteor);
	    siteor.setOnMouseClicked(e -> {
		portfolio.setSelectedPage(page);
		this.reloadPortfolioPane();
	    });
	}
	updatePageEditToolbarControls();
    }
    
    private void initTitleControls() {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String labelPrompt = props.getProperty(LABEL_PORT_TITLE);
	titlePane = new FlowPane();
	titleLabel = new Label(labelPrompt);
	titleTextField = new TextField();
	
	titlePane.getChildren().add(titleLabel);
	titlePane.getChildren().add(titleTextField);
	
	String titlePrompt = props.getProperty(LangProp.LABEL_PORT_TITLE);
	titleTextField.setText(titlePrompt);
	
	titleTextField.textProperty().addListener(e -> {
	    portfolio.setTitle(titleTextField.getText());
	});
    }
    
    public void reloadTitleControls() {
	fileToolbarPane.getChildren().add(titlePane);
	titleTextField.setText(portfolio.getTitle());
    }
}
