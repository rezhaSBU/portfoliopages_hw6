package pg.view;

import java.io.File;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import static pg.StartConts.CSS_CLASS_BOXES;
import static pg.StartConts.CSS_CLASS_BUTN;
import static pg.StartConts.CSS_CLASS_DIALOG_BOX;
import static pg.StartConts.ENGLISH_LANG;
import static pg.StartConts.SPANISH_LANG;
import static pg.StartConts.LABEL_LANGUAGE_SELECTION_PROMPT;
import static pg.StartConts.OK_BUTTON_TEXT;
import static pg.StartConts.STYLE_SHEET_UI;

/**
 *
 * @author McKillaGorilla
 */
public class VideoDialog extends Stage {
    GridPane gBox;
    VBox vBox;
    Label videoPromptLabel;
    Button loadButton;
    Button okButton;
    TextField widthField;
    TextField heightField;
    TextField captionField;
    public VideoDialog() {
	videoPromptLabel = new Label("Select an video");
	
	okButton = new Button(OK_BUTTON_TEXT);
	loadButton = new Button("Upload");
        widthField = new TextField("Width");
        heightField = new TextField("Height");
        captionField = new TextField("Caption");
        
	gBox = new GridPane();
        vBox = new VBox(20);
        gBox.setVgap(8);
        gBox.setHgap(8);
	vBox.getChildren().add(videoPromptLabel);
	vBox.getChildren().add(loadButton);
	vBox.getChildren().add(widthField);
	vBox.getChildren().add(heightField);
	vBox.getChildren().add(captionField);
	gBox.add(vBox, 0, 0, 2, 1);
        gBox.add(okButton, 1, 2);
        GridPane.setHalignment(okButton, HPos.RIGHT);
        
        loadButton.setOnAction(e -> {
	    FileChooser videoChooser = new FileChooser();
             
            //Set extension filter
            FileChooser.ExtensionFilter extFilterMP4 = new FileChooser.ExtensionFilter("MP4 files (*.mp4)", "*.MP4");
            videoChooser.getExtensionFilters().addAll(extFilterMP4);
              
            //Show open file dialog
            File file = videoChooser.showOpenDialog(null);
	});
        
	okButton.setOnAction(e -> {
	    this.hide();
	});
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gBox);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        gBox.getStyleClass().add(CSS_CLASS_DIALOG_BOX);
        vBox.getStyleClass().add(CSS_CLASS_BOXES);
	okButton.getStyleClass().add(CSS_CLASS_BUTN);
	this.setTitle("Video Component");
	setScene(scene);
    }
    
    public String getSelectedVideo() {
	return captionField.getText().toString();
    }
}
