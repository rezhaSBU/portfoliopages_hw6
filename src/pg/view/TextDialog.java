package pg.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static pg.StartConts.CSS_CLASS_BOXES;
import static pg.StartConts.CSS_CLASS_BUTN;
import static pg.StartConts.CSS_CLASS_DIALOG_BOX;
import static pg.StartConts.OK_BUTTON_TEXT;
import static pg.StartConts.STYLE_SHEET_UI;

/**
 *
 * @author McKillaGorilla
 */
public class TextDialog extends Stage {
    GridPane gBox;
    VBox vBox;
    Label textPromptLabel;
    ComboBox textComboBox;
    Button okButton;
    String selectedType = "Paragraph";
    TextField headingTextField;
    TextArea paragraphTextField;
    Button listAddButton;
    TextField listTextField;
    public TextDialog() {
	textPromptLabel = new Label("Select a text type");
	
	// INIT THE LANGUAGE CHOICES
	ObservableList<String> textChoices = FXCollections.observableArrayList();
	textChoices.add("Heading");
	textChoices.add("Paragraph");
	textChoices.add("List");
        headingTextField = new TextField();
        paragraphTextField = new TextArea();
        listTextField = new TextField();
	textComboBox = new ComboBox(textChoices);
        textComboBox.getSelectionModel().select("Heading");
	listAddButton = new Button("Add list item");
	okButton = new Button(OK_BUTTON_TEXT);
	
	gBox = new GridPane();
        vBox = new VBox(20);
        gBox.setVgap(8);
        gBox.setHgap(8);
	vBox.getChildren().add(textPromptLabel);
	vBox.getChildren().add(textComboBox);
	gBox.add(vBox, 0, 0, 2, 1);
        gBox.add(okButton, 1, 2);
        GridPane.setHalignment(okButton, HPos.RIGHT);
        
        textComboBox.setOnAction(e -> {
            String type = textComboBox.getSelectionModel().getSelectedItem().toString();
            switch (type) {
                case "Heading":
                    for (Node node : vBox.getChildren()) {
                        if (node instanceof TextField || node instanceof TextArea || node.equals(listAddButton)) {
                            vBox.getChildren().remove(node);
                        }
                    }
                    vBox.getChildren().add(headingTextField);
                    break;
                case "Paragraph":
                    for (Node node : vBox.getChildren()) {
                        if (node instanceof TextField || node instanceof TextArea || node.equals(listAddButton)) {
                            vBox.getChildren().remove(node);
                        }
                    }                    
                    vBox.getChildren().add(paragraphTextField);
                    break; 
                case "List":
                    for (Node node : vBox.getChildren()) {
                        if (node instanceof TextField || node instanceof TextArea || node.equals(listAddButton)) {
                            vBox.getChildren().remove(node);
                        }
                    }
                    vBox.getChildren().add(listAddButton);
                    vBox.getChildren().add(listTextField);
                    listAddButton.setOnAction(f -> {
                        vBox.getChildren().add(listTextField);
                    });
                    break;
            }
        });
        
	okButton.setOnAction(e -> {
	    selectedType = textComboBox.getSelectionModel().getSelectedItem().toString();
	    this.hide();
	});
	
	// NOW SET THE SCENE IN THIS WINDOW
	Scene scene = new Scene(gBox);
        scene.getStylesheets().add(STYLE_SHEET_UI);
        gBox.getStyleClass().add(CSS_CLASS_DIALOG_BOX);
        vBox.getStyleClass().add(CSS_CLASS_BOXES);
	textComboBox.getStyleClass().add(CSS_CLASS_BUTN);
	okButton.getStyleClass().add(CSS_CLASS_BUTN);
	this.setTitle("Text Component");
	setScene(scene);
    }
    
    public String getSelectedText() {
	return selectedType;
    }
}
